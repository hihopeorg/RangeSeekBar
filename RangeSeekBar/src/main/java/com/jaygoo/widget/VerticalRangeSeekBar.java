package com.jaygoo.widget;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.biometrics.authentication.IFaceAuthentication;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * ================================================
 * 作    者：JayGoo
 * 版    本：
 * 创建日期：2018/5/10
 * 描    述:
 * ================================================
 */
public class VerticalRangeSeekBar extends RangeSeekBar {

    //text direction of VerticalRangeSeekBar. include indicator and tickMark
//
//    /**
//     * @hide
//     */
//    @IntDef({TEXT_DIRECTION_VERTICAL, TEXT_DIRECTION_HORIZONTAL})
//    @Retention(RetentionPolicy.SOURCE)
//    public @interface TextDirectionDef {
//    }

    public final static int TEXT_DIRECTION_VERTICAL = 1;
    public final static int TEXT_DIRECTION_HORIZONTAL = 2;

    //direction of VerticalRangeSeekBar

//    /**
//     * @hide
//     */
//    @IntDef({DIRECTION_LEFT, DIRECTION_RIGHT})
//    @Retention(RetentionPolicy.SOURCE)
//    public @interface DirectionDef {
//    }

    public final static int DIRECTION_LEFT = 1;
    public final static int DIRECTION_RIGHT = 2;

    private int orientation = DIRECTION_LEFT;
    private int tickMarkDirection = TEXT_DIRECTION_VERTICAL;

    private int maxTickMarkWidth;

    public VerticalRangeSeekBar(Context context) {
        super(context);
        leftSB = new VerticalSeekBar(this, true);
        rightSB = new VerticalSeekBar(this,  false);
        rightSB.setVisible(getSeekBarMode() != SEEKBAR_MODE_SINGLE);
    }

    public VerticalRangeSeekBar(Context context, AttrSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        initSeekBar(attrs);
    }

    private void initAttrs(AttrSet attrs) {
        orientation = getCustomGravity(AttrSetUtil.getString(attrs, AttrConstant.RSB_ORIENTATION, "left"));
        tickMarkDirection = getCustomOrientation(AttrSetUtil.getString(attrs, AttrConstant.RSB_TICK_MARK_ORIENTATION, "vertical"));
    }

    private int getCustomGravity(String gravity) {
        if ("right".equals(gravity)) {
            return DIRECTION_RIGHT;
        }
        return DIRECTION_LEFT;
    }

    private int getCustomOrientation(String orientation) {
        if ("horizontal".equals(orientation)) {
            return TEXT_DIRECTION_HORIZONTAL;
        }
        return TEXT_DIRECTION_VERTICAL;
    }

    protected void initSeekBar(AttrSet attrs) {
        leftSB = new VerticalSeekBar(this, attrs, true);
        rightSB = new VerticalSeekBar(this, attrs, false);
        rightSB.setVisible(getSeekBarMode() != SEEKBAR_MODE_SINGLE);
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        return super.onArrange(i, i1, i3, i2);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        /*
         * onMeasure传入的widthMeasureSpec和heightMeasureSpec不是一般的尺寸数值，而是将模式和尺寸组合在一起的数值
         * MeasureSpec.EXACTLY 是精确尺寸
         * MeasureSpec.AT_MOST 是最大尺寸
         * MeasureSpec.UNSPECIFIED 是未指定尺寸
         */
        if (widthMode == EstimateSpec.PRECISE) {
            widthSize = makeMeasureSpec(widthSize, EstimateSpec.PRECISE);
        } else if (widthMode != EstimateSpec.NOT_EXCEED && this.getComponentParent() instanceof ComponentContainer
                && widthSize == ComponentContainer.LayoutConfig.MATCH_PARENT) {
            widthSize = makeMeasureSpec(((ComponentContainer) getComponentParent()).getEstimatedHeight(), EstimateSpec.NOT_EXCEED);
            ;
        } else {
            int heightNeeded;
            if (getGravity() == Gravity.CENTER) {
                heightNeeded = 2 * getProgressTop() + getProgressHeight();
            } else {
                heightNeeded = (int) getRawHeight();
            }
            widthSize = makeMeasureSpec(heightNeeded, EstimateSpec.PRECISE);
        }
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(widthSize, widthSize, widthMode),
                Component.EstimateSpec.getChildSizeWithMode(heightMeasureSpec, heightMeasureSpec, heightMode)
        );
        return super.onEstimateSize(widthSize, heightMeasureSpec);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (orientation == DIRECTION_LEFT) {
            canvas.rotate(-90);
            canvas.translate(-getHeight(), 0);
        } else {
            canvas.rotate(90);
            canvas.translate(0, -getWidth());
        }
        super.onDraw(component, canvas);
    }

    @Override
    protected void onDrawTickMark(Canvas canvas, Paint paint) {
        if (getTickMarkTextArray() != null) {
            int arrayLength = getTickMarkTextArray().length;
            int trickPartWidth = getProgressWidth() / (arrayLength - 1);
            for (int i = 0; i < arrayLength; i++) {
                final String text2Draw = getTickMarkTextArray()[i].toString();
                if (TextTool.isNullOrEmpty(text2Draw)) continue;
                tickMarkTextRect = paint.getTextBounds(text2Draw);
                paint.setColor(new Color(getTickMarkTextColor()));
                //平分显示
                float x;
                if (getTickMarkMode() == TRICK_MARK_MODE_OTHER) {
                    if (getTickMarkGravity() == TICK_MARK_GRAVITY_RIGHT) {
                        x = getProgressLeft() + i * trickPartWidth - tickMarkTextRect.getWidth();
                    } else if (getTickMarkGravity() == TICK_MARK_GRAVITY_CENTER) {
                        x = getProgressLeft() + i * trickPartWidth - tickMarkTextRect.getWidth() / 2f;
                    } else {
                        x = getProgressLeft() + i * trickPartWidth;
                    }
                } else {
                    float num = Utils.parseFloat(text2Draw);
                    SeekBarState[] states = getRangeSeekBarState();
                    if (Utils.compareFloat(num, states[0].value) != -1 && Utils.compareFloat(num, states[1].value) != 1 && (getSeekBarMode() == SEEKBAR_MODE_RANGE)) {
                        paint.setColor(new Color(getTickMarkInRangeTextColor()));
                    }
                    //按实际比例显示
                    x = getProgressLeft() + getProgressWidth() * (num - getMinProgress()) / (getMaxProgress() - getMinProgress())
                            - tickMarkTextRect.getWidth() / 2f;
                }
                float y;
                if (getTickMarkLayoutGravity() == Gravity.TOP) {
                    y = getProgressTop() - getTickMarkTextMargin();
                } else {
                    y = getProgressBottom() + getTickMarkTextMargin() + tickMarkTextRect.getHeight();
                }
                int degrees = 0;
                float rotateX = (x + tickMarkTextRect.getWidth() / 2f);
                float rotateY = (y - tickMarkTextRect.getHeight() / 2f);
                if (tickMarkDirection == TEXT_DIRECTION_VERTICAL) {
                    if (orientation == DIRECTION_LEFT) {
                        degrees = 90;
                    } else if (orientation == DIRECTION_RIGHT) {
                        degrees = -90;
                    }
                }
                if (degrees != 0) {
                    canvas.rotate(degrees, rotateX, rotateY);
                }
                canvas.drawText(paint, text2Draw, x, y);
                if (degrees != 0) {
                    canvas.rotate(-degrees, rotateX, rotateY);
                }
            }
        }

    }


    @Override
    protected int getTickMarkRawHeight() {
        if (maxTickMarkWidth > 0) return getTickMarkTextMargin() + maxTickMarkWidth;
        if (getTickMarkTextArray() != null && getTickMarkTextArray().length > 0) {
            int arrayLength = getTickMarkTextArray().length;
            maxTickMarkWidth = Utils.measureText(String.valueOf(getTickMarkTextArray()[0]), getTickMarkTextSize()).getWidth();
            for (int i = 1; i < arrayLength; i++) {
                int width = Utils.measureText(String.valueOf(getTickMarkTextArray()[i]), getTickMarkTextSize()).getWidth();
                if (maxTickMarkWidth < width) {
                    maxTickMarkWidth = width;
                }
            }
            return getTickMarkTextMargin() + maxTickMarkWidth;
        }
        return 0;
    }

    @Override
    public void setTickMarkTextSize(int tickMarkTextSize) {
        super.setTickMarkTextSize(tickMarkTextSize);
        maxTickMarkWidth = 0;
    }

    @Override
    public void setTickMarkTextArray(String[] tickMarkTextArray) {
        super.setTickMarkTextArray(tickMarkTextArray);
        maxTickMarkWidth = 0;
    }

    @Override
    protected float getEventX(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(0);
        if (orientation == DIRECTION_LEFT) {
            return getHeight() - point.getY();
        } else {
            return point.getY();
        }
    }

    @Override
    protected float getEventY(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(0);
        if (orientation == DIRECTION_LEFT) {
            return point.getX();
        } else {
            return -point.getX() + getWidth();
        }
    }

    /**
     * if is single mode, please use it to get the SeekBar
     *
     * @return left seek bar
     */
    public VerticalSeekBar getLeftSeekBar() {
        return (VerticalSeekBar) leftSB;
    }

    public VerticalSeekBar getRightSeekBar() {
        return (VerticalSeekBar) rightSB;
    }

    public int getOrientation() {
        return orientation;
    }

    /**
     * set VerticalRangeSeekBar Orientation
     * {@link #DIRECTION_LEFT}
     * {@link #DIRECTION_RIGHT}
     *
     * @param orientation
     */
    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getTickMarkDirection() {
        return tickMarkDirection;
    }

    /**
     * set tick mark text direction
     * {@link #TEXT_DIRECTION_VERTICAL}
     * {@link #TEXT_DIRECTION_HORIZONTAL}
     *
     * @param tickMarkDirection
     */
    public void setTickMarkDirection(int tickMarkDirection) {
        this.tickMarkDirection = tickMarkDirection;
    }
}
