package com.jaygoo.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

import static com.jaygoo.widget.VerticalRangeSeekBar.*;


/**
 * //                       _ooOoo_
 * //                      o8888888o
 * //                      88" . "88
 * //                      (| -_- |)
 * //                       O\ = /O
 * //                   ____/`---'\____
 * //                 .   ' \\| |// `.
 * //                  / \\||| : |||// \
 * //                / _||||| -:- |||||- \
 * //                  | | \\\ - /// | |
 * //                | \_| ''\---/'' | |
 * //                 \ .-\__ `-` ___/-. /
 * //              ______`. .' /--.--\ `. . __
 * //           ."" '< `.___\_<|>_/___.' >'"".
 * //          | | : `- \`.;`\ _ /`;.`/ - ` : | |
 * //            \ \ `-. \_ __\ /__ _/ .-` / /
 * //    ======`-.____`-.___\_____/___.-`____.-'======
 * //                       `=---='
 * //
 * //    .............................................
 * //             佛祖保佑             永无BUG
 * =====================================================
 * 作    者：JayGoo
 * 创建日期：2019-06-05
 * 描    述:
 * =====================================================
 */
public class VerticalSeekBar extends SeekBar {

    private int indicatorTextOrientation = TEXT_DIRECTION_VERTICAL;
    VerticalRangeSeekBar verticalSeekBar;

    public VerticalSeekBar(RangeSeekBar rangeSeekBar, AttrSet attrs, boolean isLeft) {
        super(rangeSeekBar, attrs, isLeft);
        initAttrs(attrs);
        verticalSeekBar = (VerticalRangeSeekBar) rangeSeekBar;
    }

    public VerticalSeekBar(RangeSeekBar rangeSeekBar, boolean isLeft) {
        super(rangeSeekBar, isLeft);
        verticalSeekBar = (VerticalRangeSeekBar) rangeSeekBar;
    }

    private void initAttrs(AttrSet attrs) {
        indicatorTextOrientation = getCustomOrientation(AttrSetUtil.getString(attrs, AttrConstant.RSB_INDICATOR_TEXT_ORIENTATION, "vertical"));

    }

    private int getCustomOrientation(String orientation) {
        if ("horizontal".equals(orientation)) {
            return TEXT_DIRECTION_HORIZONTAL;
        }
        return TEXT_DIRECTION_VERTICAL;
    }

    @Override
    protected void onDrawIndicator(Canvas canvas, Paint paint, String text2Draw) {
        if (text2Draw == null) return;
        //draw indicator
        if (indicatorTextOrientation == TEXT_DIRECTION_VERTICAL) {
            drawVerticalIndicator(canvas, paint, text2Draw);
        } else {
            super.onDrawIndicator(canvas, paint, text2Draw);
        }
    }

    protected void drawVerticalIndicator(Canvas canvas, Paint paint, String text2Draw) {
        //measure indicator text
        paint.setTextSize(getIndicatorTextSize());
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(new Color(getIndicatorBackgroundColor()));
        indicatorTextRect = paint.getTextBounds(text2Draw);

        int realIndicatorWidth = (indicatorTextRect.getHeight() + getIndicatorPaddingLeft() + getIndicatorPaddingRight());
        if (getIndicatorWidth() > realIndicatorWidth) {
            realIndicatorWidth = getIndicatorWidth();
        }

        int realIndicatorHeight = (indicatorTextRect.getWidth() + getIndicatorPaddingTop() + getIndicatorPaddingBottom());
        if (getIndicatorHeight() > realIndicatorHeight) {
            realIndicatorHeight = getIndicatorHeight();
        }

        indicatorRect.left = scaleThumbWidth / 2 - realIndicatorWidth / 2;
        indicatorRect.top = bottom - realIndicatorHeight - scaleThumbHeight - getIndicatorMargin();
        indicatorRect.right = indicatorRect.left + realIndicatorWidth;
        indicatorRect.bottom = indicatorRect.top + realIndicatorHeight;

        //draw default indicator arrow
        if (indicatorBitmap == null) {
            //arrow three point
            //  b   c
            //    a
            int ax = scaleThumbWidth / 2;
            int ay = (int) indicatorRect.bottom;
            int bx = ax - getIndicatorArrowSize();
            int by = ay - getIndicatorArrowSize();
            int cx = ax + getIndicatorArrowSize();
            indicatorArrowPath.reset();
            indicatorArrowPath.moveTo(ax, ay);
            indicatorArrowPath.lineTo(bx, by);
            indicatorArrowPath.lineTo(cx, by);
            indicatorArrowPath.close();
            canvas.drawPath(indicatorArrowPath, paint);
            indicatorRect.bottom -= getIndicatorArrowSize();
            indicatorRect.top -= getIndicatorArrowSize();
        }

        int defaultPaddingOffset = 3;
        int leftOffset = (int) (indicatorRect.getWidth() / 2 - (int) (rangeSeekBar.getProgressWidth() * currPercent) - rangeSeekBar.getProgressLeft() + defaultPaddingOffset);
        int rightOffset = (int) (indicatorRect.getWidth() / 2 - (int) (rangeSeekBar.getProgressWidth() * (1 - currPercent)) - rangeSeekBar.getProgressPaddingRight() + defaultPaddingOffset);
        if (leftOffset > 0) {
            indicatorRect.left += leftOffset;
            indicatorRect.right += leftOffset;
        } else if (rightOffset > 0) {
            indicatorRect.left -= rightOffset;
            indicatorRect.right -= rightOffset;
        }

        //draw indicator background
        if (getIndicatorDrawableId() != null) {
            if (indicatorBitmap != null) {
                Utils.drawBitmap(canvas, paint, indicatorBitmap, indicatorRect);
            }
        } else if (getIndicatorRadius() > 0f) {
            canvas.drawRoundRect(new RectFloat(indicatorRect), getIndicatorRadius(), getIndicatorRadius(), paint);
        } else {
            canvas.drawRect(indicatorRect, paint);
        }

        //draw indicator content text
        int tx = (int) (indicatorRect.left + (indicatorRect.getWidth() - indicatorTextRect.getWidth()) / 2 + getIndicatorPaddingLeft() - getIndicatorPaddingRight());
        int ty = (int) (indicatorRect.bottom - (indicatorRect.getHeight() - indicatorTextRect.getHeight()) / 2 + getIndicatorPaddingTop() - getIndicatorPaddingBottom());

        //draw indicator text
        paint.setColor(new Color(getIndicatorTextColor()));

        int degrees = 0;
        float rotateX = (tx + indicatorTextRect.getWidth() / 2f);
        float rotateY = (ty - indicatorTextRect.getHeight() / 2f);

        if (indicatorTextOrientation == TEXT_DIRECTION_VERTICAL) {
            if (verticalSeekBar.getOrientation() == DIRECTION_LEFT) {
                degrees = 90;
            } else if (verticalSeekBar.getOrientation() == DIRECTION_RIGHT) {
                degrees = -90;
            }
        }
        if (degrees != 0) {
            canvas.rotate(degrees, rotateX, rotateY);
        }
        canvas.drawText(paint, text2Draw, tx, ty);
        if (degrees != 0) {
            canvas.rotate(-degrees, rotateX, rotateY);
        }
    }

    public int getIndicatorTextOrientation() {
        return indicatorTextOrientation;
    }

    public void setIndicatorTextOrientation(int orientation) {
        this.indicatorTextOrientation = orientation;
    }
}
