package com.jaygoo.widget;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.text.Font;
import ohos.agp.utils.*;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.*;

import java.text.DecimalFormat;
import java.util.Optional;


/**
 * ================================================
 * 作    者：JayGoo
 * 版    本：
 * 创建日期：2018/5/8
 * 描    述:
 * ================================================
 */

public class SeekBar {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000101, "SeekBar");
    //the indicator show mode
    public static final int INDICATOR_SHOW_WHEN_TOUCH = 0;
    public static final int INDICATOR_ALWAYS_HIDE = 1;
    public static final int INDICATOR_ALWAYS_SHOW_AFTER_TOUCH = 2;
    public static final int INDICATOR_ALWAYS_SHOW = 3;

    public static final int WRAP_CONTENT = -1;
    public static final int MATCH_PARENT = -2;

    private int indicatorShowMode = INDICATOR_ALWAYS_HIDE;

    //进度提示背景的高度，宽度如果是0的话会自适应调整
    //Progress prompted the background height, width,
    private int indicatorHeight;
    private int indicatorWidth;
    //进度提示背景与按钮之间的距离
    //The progress indicates the distance between the background and the button
    private int indicatorMargin;
    private Element indicatorDrawable;
    private int indicatorArrowSize;
    private int indicatorTextSize = 42;
    private int indicatorTextColor = Color.WHITE.getValue();
    private float indicatorRadius;
    private int indicatorBackgroundColor = Color.getIntColor("#2ed184");
    private int indicatorPaddingLeft, indicatorPaddingRight, indicatorPaddingTop, indicatorPaddingBottom;
    private Element thumbDrawable;
    private Element thumbInactivatedDrawable ;
    private int thumbWidth = 72;
    private int thumbHeight = 72;

    //when you touch or move, the thumb will scale, default not scale
    float thumbScaleRatio = 1.0f;

    //****************** the above is attr value  ******************//

    int left, right, top, bottom;
    float currPercent;
    float material = 0;
    private boolean isShowIndicator;
    boolean isLeft;
    PixelMap thumbBitmap;
    PixelMap thumbInactivatedBitmap;
    PixelMap indicatorBitmap;
    AnimatorValue anim;
    String userText2Draw;
    boolean isActivate = false;
    boolean isVisible = true;
    RangeSeekBar rangeSeekBar;
    String indicatorTextStringFormat;
    Path indicatorArrowPath = new Path();
    Rect indicatorTextRect = new Rect();
    RectFloat indicatorRect = new RectFloat();
    Paint paint = new Paint();
    DecimalFormat indicatorTextDecimalFormat;
    int scaleThumbWidth;
    int scaleThumbHeight;

    public SeekBar(RangeSeekBar rangeSeekBar, AttrSet attrs, boolean isLeft) {
        this.rangeSeekBar = rangeSeekBar;
        this.isLeft = isLeft;
        initAttrs(attrs);
        initBitmap();
        initVariables();
    }

    public SeekBar(RangeSeekBar rangeSeekBar , boolean isLeft) {
        this.rangeSeekBar = rangeSeekBar;
        this.isLeft = isLeft;
        thumbInactivatedDrawable = ElementScatter.getInstance(rangeSeekBar.getContext()).parse(ResourceTable.Graphic_rsb_default_thumb);
        initBitmap();
        initVariables();
    }
    private void initAttrs(AttrSet attrs) {
        if (attrs == null) return;

        indicatorMargin = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_MARGIN, 0);
        indicatorDrawable = AttrSetUtil.getElement(attrs, AttrConstant.RSB_INDICATOR_DRAWABLE);
        indicatorShowMode = getIndicatorMode(AttrSetUtil.getString(attrs, AttrConstant.RSB_INDICATOR_SHOW_MODE, "alwaysHide"));
        indicatorHeight = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_HEIGHT, WRAP_CONTENT);
        indicatorWidth = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_WEIGHT, WRAP_CONTENT);
        indicatorTextSize = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_TEXT_SIZE, 42);
        indicatorTextColor = AttrSetUtil.getColor(attrs, AttrConstant.RSB_INDICATOR_TEXT_COLOR, Color.WHITE);
        indicatorBackgroundColor = AttrSetUtil.getColor(attrs, AttrConstant.RSB_INDICATOR_TEXT_COLOR, new Color(Color.getIntColor("#2ed184")));
        indicatorPaddingLeft = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_PADDING_LEFT, 0);
        indicatorPaddingRight = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_PADDING_RIGHT, 0);
        indicatorPaddingTop = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_PADDING_TOP, 0);
        indicatorPaddingBottom = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_PADDING_BOTTOM, 0);
        indicatorArrowSize = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_ARROW_SIZE, 0);
        thumbDrawable = AttrSetUtil.getElement(attrs, AttrConstant.RSB_THUMB_DRAWABLE);
        if (thumbDrawable == null) {
            thumbDrawable = ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_rsb_default_thumb);
        }
        thumbInactivatedDrawable = AttrSetUtil.getElement(attrs, AttrConstant.RSB_THUMB_INACTIVATED_DRAWABLE);
        thumbWidth = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_THUMB_WIDTH, 72);
        thumbHeight = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_THUMB_HEIGHT, 72);
        thumbScaleRatio = (int) AttrSetUtil.getFloat(attrs, AttrConstant.RSB_THUMB_SCALE_RATIO, 1f);
        indicatorRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.RSB_INDICATOR_RADIUS, 0);
    }

    protected void initVariables() {
        scaleThumbWidth = thumbWidth;
        scaleThumbHeight = thumbHeight;
        if (indicatorHeight == WRAP_CONTENT) {
            indicatorHeight = Utils.measureText("8", indicatorTextSize).getHeight() + indicatorPaddingTop + indicatorPaddingBottom;
        }
        if (indicatorArrowSize <= 0) {
            indicatorArrowSize = (int) (thumbWidth / 4);
        }
    }

    private int getIndicatorMode(String mode) {
        if ("showWhenTouch".equals(mode)) {
            return INDICATOR_SHOW_WHEN_TOUCH;
        } else if ("alwaysShowAfterTouch".equals(mode)) {
            return INDICATOR_ALWAYS_SHOW_AFTER_TOUCH;
        } else if ("alwaysShow".equals(mode)) {
            return INDICATOR_ALWAYS_SHOW;
        } else {
            return INDICATOR_ALWAYS_HIDE;
        }
    }

    public Context getContext() {
        return rangeSeekBar.getContext();
    }

    public ResourceManager getResources() {
        if (getContext() != null) return getContext().getResourceManager();
        return null;
    }

    /**
     * 初始化进度提示的背景
     */
    private void initBitmap() {
        setIndicatorDrawableId(indicatorDrawable);
        setThumbDrawableId(thumbDrawable, thumbWidth, thumbHeight);
        setThumbInactivatedDrawableId(thumbInactivatedDrawable, thumbWidth, thumbHeight);
    }

    /**
     * 计算每个按钮的位置和尺寸
     * Calculates the position and size of each button
     *
     * @param x position x
     * @param y position y
     */
    protected void onSizeChanged(int x, int y) {
        initVariables();
        initBitmap();
        left = (int) (x - getThumbScaleWidth() / 2);
        right = (int) (x + getThumbScaleWidth() / 2);
        top = y - getThumbHeight() / 2;
        bottom = y + getThumbHeight() / 2;
    }


    public void scaleThumb() {
        scaleThumbWidth = (int) getThumbScaleWidth();
        scaleThumbHeight = (int) getThumbScaleHeight();
        int y = rangeSeekBar.getProgressBottom();
        top = y - scaleThumbHeight / 2;
        bottom = y + scaleThumbHeight / 2;
        setThumbDrawableId(thumbDrawable, scaleThumbWidth, scaleThumbHeight);
    }

    public void resetThumb() {
        scaleThumbWidth = getThumbWidth();
        scaleThumbHeight = getThumbHeight();
        int y = rangeSeekBar.getProgressBottom();
        top = y - scaleThumbHeight / 2;
        bottom = y + scaleThumbHeight / 2;
        setThumbDrawableId(thumbDrawable, scaleThumbWidth, scaleThumbHeight);
    }

    public float getRawHeight() {
        return getIndicatorHeight() + getIndicatorArrowSize() + getIndicatorMargin() + getThumbScaleHeight();
    }

    /**
     * 绘制按钮和提示背景和文字
     * Draw buttons and tips for background and text
     *
     * @param canvas Canvas
     */
    protected void draw(Canvas canvas) {
        if (!isVisible) {
            return;
        }
        int offset = (int) (rangeSeekBar.getProgressWidth() * currPercent);
        canvas.save();
        canvas.translate(offset, 0);
        // translate canvas, then don't care left
        canvas.translate(left, 0);
        if (isShowIndicator) {
            onDrawIndicator(canvas, paint, formatCurrentIndicatorText(userText2Draw));
        }
        onDrawThumb(canvas);
        canvas.restore();
    }


    /**
     * 绘制按钮
     * 如果没有图片资源，则绘制默认按钮
     * <p>
     * draw the thumb button
     * If there is no image resource, draw the default button
     *
     * @param canvas canvas
     */
    protected void onDrawThumb(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        float elementTop = rangeSeekBar.getProgressTop() + (rangeSeekBar.getProgressHeight() - scaleThumbHeight) / 2f;
        if (thumbInactivatedDrawable != null && !isActivate) {
            if (thumbInactivatedBitmap != null) {
                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                initializationOptions.size = new Size(scaleThumbWidth, scaleThumbHeight);
                thumbInactivatedBitmap = PixelMap.create(thumbInactivatedBitmap, initializationOptions);
                canvas.drawPixelMapHolder(new PixelMapHolder(thumbInactivatedBitmap), 0, elementTop, paint);
            } else {
                thumbInactivatedDrawable.setBounds(0, (int) elementTop, scaleThumbWidth, (int) (scaleThumbHeight + elementTop));
                thumbInactivatedDrawable.drawToCanvas(canvas);
            }
        } else if (thumbDrawable != null) {
            if (thumbBitmap != null) {
                ImageInfo info = thumbBitmap.getImageInfo();
                float scaleWidth = thumbWidth * 1.0f / info.size.width;
                float scaleHeight = thumbHeight * 1.0f / info.size.height;
                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                initializationOptions.size = new Size(thumbWidth, thumbHeight);
                initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
                thumbBitmap = PixelMap.create(thumbBitmap, initializationOptions);
                canvas.drawPixelMapHolder(new PixelMapHolder(thumbBitmap), 0, elementTop, paint);
            } else {
                thumbDrawable.setBounds(0, (int) elementTop, thumbWidth, (int) (scaleThumbHeight + elementTop));
                thumbDrawable.drawToCanvas(canvas);
            }

        }
    }

    /**
     * 格式化提示文字
     * format the indicator text
     *
     * @param text2Draw
     * @return
     */
    protected String formatCurrentIndicatorText(String text2Draw) {
        SeekBarState[] states = rangeSeekBar.getRangeSeekBarState();
        if (TextTool.isNullOrEmpty(text2Draw)) {
            if (isLeft) {
                if (indicatorTextDecimalFormat != null) {
                    text2Draw = indicatorTextDecimalFormat.format(states[0].value);
                } else {
                    text2Draw = states[0].indicatorText;
                }
            } else {
                if (indicatorTextDecimalFormat != null) {
                    text2Draw = indicatorTextDecimalFormat.format(states[1].value);
                } else {
                    text2Draw = states[1].indicatorText;
                }
            }
        }
        if (indicatorTextStringFormat != null) {
            text2Draw = String.format(indicatorTextStringFormat, text2Draw);
        }
        return text2Draw;
    }

    /**
     * This method will draw the indicator background dynamically according to the text.
     * you can use to set padding
     *
     * @param canvas    Canvas
     * @param text2Draw Indicator text
     */
    protected void onDrawIndicator(Canvas canvas, Paint paint, String text2Draw) {
        if (text2Draw == null) return;
        paint.setTextSize(indicatorTextSize);
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(new Color(indicatorBackgroundColor));
        Rect rect = paint.getTextBounds(text2Draw);
        indicatorTextRect.set(rect.left, rect.top, rect.right, rect.bottom);
        int realIndicatorWidth = (int) (indicatorTextRect.getWidth() + indicatorPaddingLeft + indicatorPaddingRight);
        if (indicatorWidth > realIndicatorWidth) {
            realIndicatorWidth = indicatorWidth;
        }

        int realIndicatorHeight = (int) (indicatorTextRect.getHeight() + indicatorPaddingTop + indicatorPaddingBottom);
        if (indicatorHeight > realIndicatorHeight) {
            realIndicatorHeight = indicatorHeight;
        }

        indicatorRect.left = (int) (scaleThumbWidth / 2f - realIndicatorWidth / 2f);
        indicatorRect.top = bottom - realIndicatorHeight - scaleThumbHeight - indicatorMargin;
        indicatorRect.right = indicatorRect.left + realIndicatorWidth;
        indicatorRect.bottom = indicatorRect.top + realIndicatorHeight;
        //draw default indicator arrow
        if (indicatorBitmap == null) {
            //arrow three point
            //  b   c
            //    a
            int ax = scaleThumbWidth / 2;
            int ay = (int) indicatorRect.bottom;
            int bx = ax - indicatorArrowSize;
            int by = ay - indicatorArrowSize;
            int cx = ax + indicatorArrowSize;
            indicatorArrowPath.reset();
            indicatorArrowPath.moveTo(ax, ay);
            indicatorArrowPath.lineTo(bx, by);
            indicatorArrowPath.lineTo(cx, by);
            indicatorArrowPath.close();
            canvas.drawPath(indicatorArrowPath, paint);
            indicatorRect.bottom -= indicatorArrowSize;
            indicatorRect.top -= indicatorArrowSize;
        }

        //indicator background edge processing
        int defaultPaddingOffset = 3;
        float leftOffset = indicatorRect.getWidth() / 2 - (int) (rangeSeekBar.getProgressWidth() * currPercent) - rangeSeekBar.getProgressLeft() + defaultPaddingOffset;
        float rightOffset = indicatorRect.getWidth() / 2 - (int) (rangeSeekBar.getProgressWidth() * (1 - currPercent)) - rangeSeekBar.getProgressPaddingRight() + defaultPaddingOffset;
        if (leftOffset > 0) {
            indicatorRect.left += leftOffset;
            indicatorRect.right += leftOffset;
        } else if (rightOffset > 0) {
            indicatorRect.left -= rightOffset;
            indicatorRect.right -= rightOffset;
        }

        //draw indicator background
        if (indicatorBitmap != null) {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(indicatorWidth, indicatorHeight);
            indicatorBitmap = PixelMap.create(indicatorBitmap, initializationOptions);
            canvas.drawPixelMapHolder(new PixelMapHolder(indicatorBitmap), indicatorRect.left, indicatorRect.top, paint);
//            Utils.drawBitmap(canvas, paint, indicatorBitmap, indicatorRect);
        } else if (indicatorDrawable != null) {
            indicatorDrawable.setBounds((int) indicatorRect.left, (int) indicatorRect.top, (int) indicatorRect.right, (int) indicatorRect.bottom);
            indicatorDrawable.drawToCanvas(canvas);
        } else if (indicatorRadius > 0f) {
            canvas.drawRoundRect(new RectFloat(indicatorRect), indicatorRadius, indicatorRadius, paint);
        } else {
            canvas.drawRect(indicatorRect, paint);
        }

        //draw indicator content text
        float tx, ty;
        if (indicatorPaddingLeft > 0) {
            tx = indicatorRect.left + indicatorPaddingLeft;
        } else if (indicatorPaddingRight > 0) {
            tx = indicatorRect.right - indicatorPaddingRight - indicatorTextRect.getWidth();
        } else {
            tx = indicatorRect.left + (realIndicatorWidth - indicatorTextRect.getWidth()) / 2;
        }

        if (indicatorPaddingTop > 0) {
            ty = indicatorRect.top + indicatorTextRect.getHeight() + indicatorPaddingTop;
        } else if (indicatorPaddingBottom > 0) {
            ty = indicatorRect.bottom - indicatorTextRect.getHeight() - indicatorPaddingBottom;
        } else {
            ty = indicatorRect.bottom - (realIndicatorHeight - indicatorTextRect.getHeight()) / 2 + 1;
        }

        //draw indicator text
        paint.setColor(new Color(indicatorTextColor));
        canvas.drawText(paint, text2Draw, tx, ty);
    }

    /**
     * 拖动检测
     *
     * @return is collide
     */
    protected boolean collide(float x, float y) {
        int offset = (int) (rangeSeekBar.getProgressWidth() * currPercent);
        return x > left + offset && x < right + offset && y > top && y < bottom;
    }

    protected void slide(float percent) {
        if (percent < 0) percent = 0;
        else if (percent > 1) percent = 1;
        currPercent = percent;
    }

    protected void setShowIndicatorEnable(boolean isEnable) {
        switch (indicatorShowMode) {
            case INDICATOR_SHOW_WHEN_TOUCH:
                isShowIndicator = isEnable;
                break;
            case INDICATOR_ALWAYS_SHOW:
            case INDICATOR_ALWAYS_SHOW_AFTER_TOUCH:
                isShowIndicator = true;
                break;
            case INDICATOR_ALWAYS_HIDE:
                isShowIndicator = false;
                break;
        }
    }

    public void materialRestore() {
        if (anim != null) anim.cancel();
        anim = new AnimatorValue();
        anim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                material = v;
                if (rangeSeekBar != null) rangeSeekBar.invalidate();
            }
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                material = 0;
                if (rangeSeekBar != null) rangeSeekBar.invalidate();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    public void setIndicatorText(String text) {
        userText2Draw = text;
    }

    public void setIndicatorTextDecimalFormat(String formatPattern) {
        indicatorTextDecimalFormat = new DecimalFormat(formatPattern);
    }

    public DecimalFormat getIndicatorTextDecimalFormat() {
        return indicatorTextDecimalFormat;
    }

    public void setIndicatorTextStringFormat(String formatPattern) {
        indicatorTextStringFormat = formatPattern;
    }

    public Element getIndicatorDrawableId() {
        return indicatorDrawable;
    }

    public void setIndicatorDrawableId(Element indicatorDrawableId) {
        if (indicatorDrawableId != null) {
            this.indicatorDrawable = indicatorDrawableId;
            if (indicatorDrawable instanceof PixelMapElement) {
                PixelMapElement element = (PixelMapElement) indicatorDrawable;
                indicatorBitmap = element.getPixelMap();
            }
        }
    }

    public static PixelMap decodeResource(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            if (path.isEmpty()) {
                return null;
            }
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getIndicatorArrowSize() {
        return indicatorArrowSize;
    }

    public void setIndicatorArrowSize(int indicatorArrowSize) {
        this.indicatorArrowSize = indicatorArrowSize;
    }

    public int getIndicatorPaddingLeft() {
        return indicatorPaddingLeft;
    }

    public void setIndicatorPaddingLeft(int indicatorPaddingLeft) {
        this.indicatorPaddingLeft = indicatorPaddingLeft;
    }

    public int getIndicatorPaddingRight() {
        return indicatorPaddingRight;
    }

    public void setIndicatorPaddingRight(int indicatorPaddingRight) {
        this.indicatorPaddingRight = indicatorPaddingRight;
    }

    public int getIndicatorPaddingTop() {
        return indicatorPaddingTop;
    }

    public void setIndicatorPaddingTop(int indicatorPaddingTop) {
        this.indicatorPaddingTop = indicatorPaddingTop;
    }

    public int getIndicatorPaddingBottom() {
        return indicatorPaddingBottom;
    }

    public void setIndicatorPaddingBottom(int indicatorPaddingBottom) {
        this.indicatorPaddingBottom = indicatorPaddingBottom;
    }

    public int getIndicatorMargin() {
        return indicatorMargin;
    }

    public void setIndicatorMargin(int indicatorMargin) {
        this.indicatorMargin = indicatorMargin;
    }

    public int getIndicatorShowMode() {
        return indicatorShowMode;
    }

    /**
     * the indicator show mode
     * {@link #INDICATOR_SHOW_WHEN_TOUCH}
     * {@link #INDICATOR_ALWAYS_SHOW}
     * {@link #INDICATOR_ALWAYS_SHOW_AFTER_TOUCH}
     * {@link #INDICATOR_ALWAYS_SHOW}
     *
     * @param indicatorShowMode
     */
    public void setIndicatorShowMode(int indicatorShowMode) {
        this.indicatorShowMode = indicatorShowMode;
    }

    public void showIndicator(boolean isShown) {
        isShowIndicator = isShown;
    }

    public boolean isShowIndicator() {
        return isShowIndicator;
    }

    /**
     * include indicator text Height、padding、margin
     *
     * @return The actual occupation height of indicator
     */
    public int getIndicatorRawHeight() {
        if (indicatorHeight > 0) {
            if (indicatorBitmap != null) {
                return indicatorHeight + indicatorMargin;
            } else {
                return indicatorHeight + indicatorArrowSize + indicatorMargin;
            }
        } else {
            if (indicatorBitmap != null) {
                return Utils.measureText("8", indicatorTextSize).getHeight() + indicatorPaddingTop + indicatorPaddingBottom + indicatorMargin;
            } else {
                return Utils.measureText("8", indicatorTextSize).getHeight() + indicatorPaddingTop + indicatorPaddingBottom + indicatorMargin + indicatorArrowSize;
            }
        }
    }

    public int getIndicatorHeight() {
        return indicatorHeight;
    }

    public void setIndicatorHeight(int indicatorHeight) {
        this.indicatorHeight = indicatorHeight;
    }

    public int getIndicatorWidth() {
        return indicatorWidth;
    }

    public void setIndicatorWidth(int indicatorWidth) {
        this.indicatorWidth = indicatorWidth;
    }

    public int getIndicatorTextSize() {
        return indicatorTextSize;
    }

    public void setIndicatorTextSize(int indicatorTextSize) {
        this.indicatorTextSize = indicatorTextSize;
    }

    public int getIndicatorTextColor() {
        return indicatorTextColor;
    }

    public void setIndicatorTextColor(int indicatorTextColor) {
        this.indicatorTextColor = indicatorTextColor;
    }

    public int getIndicatorBackgroundColor() {
        return indicatorBackgroundColor;
    }

    public void setIndicatorBackgroundColor(int indicatorBackgroundColor) {
        this.indicatorBackgroundColor = indicatorBackgroundColor;
    }

    public Element getThumbInactivatedDrawableId() {
        return thumbInactivatedDrawable;
    }

    public void setThumbInactivatedDrawableId(Element thumbInactivatedDrawableId, int width, int height) {
        if (thumbInactivatedDrawableId != null && getResources() != null) {
            this.thumbInactivatedDrawable = thumbInactivatedDrawableId;
            if (thumbInactivatedDrawable instanceof PixelMapElement) {
                PixelMapElement mapElement = (PixelMapElement) thumbInactivatedDrawable;
                thumbInactivatedBitmap = mapElement.getPixelMap();
            }
        }
    }

    public Element getThumbDrawableId() {
        return thumbDrawable;
    }

    public void setThumbDrawableId(Element thumbDrawableId, int width, int height) {
        if (thumbDrawableId != null && width > 0 && height > 0) {
            this.thumbDrawable = thumbDrawableId;
            if (thumbDrawable instanceof PixelMapElement) {
                PixelMapElement mapElement = (PixelMapElement) thumbDrawable;
                thumbBitmap = mapElement.getPixelMap();
            }
        }
        HiLog.info(label, "thumbWidth,height,id,:%{public}d,%{public}d,%{public}d", width, height, thumbDrawableId);
    }

    public void setThumbDrawableId(Element thumbDrawableId) {
        if (thumbWidth <= 0 || thumbHeight <= 0) {
            throw new IllegalArgumentException("please set thumbWidth and thumbHeight first!");
        }
        if (thumbDrawableId != null) {
            this.thumbDrawable = thumbDrawableId;
            if (thumbDrawable instanceof PixelMapElement) {
                PixelMapElement mapElement = (PixelMapElement) thumbDrawable;
                thumbBitmap = mapElement.getPixelMap();
            }
        }
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public float getThumbScaleHeight() {
        return thumbHeight * thumbScaleRatio;
    }

    public float getThumbScaleWidth() {
        return thumbWidth * thumbScaleRatio;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public float getIndicatorRadius() {
        return indicatorRadius;
    }

    public void setIndicatorRadius(float indicatorRadius) {
        this.indicatorRadius = indicatorRadius;
    }

    protected boolean getActivate() {
        return isActivate;
    }

    protected void setActivate(boolean activate) {
        isActivate = activate;
    }

    public void setTypeface(Font typeFace) {
        paint.setFont(typeFace);
    }


    /**
     * when you touch or move, the thumb will scale, default not scale
     *
     * @return default 1.0f
     */
    public float getThumbScaleRatio() {
        return thumbScaleRatio;
    }

    public boolean isVisible() {
        return isVisible;
    }

    /**
     * if visble is false, will clear the Canvas
     *
     * @param visible
     */
    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public float getProgress() {
        float range = rangeSeekBar.getMaxProgress() - rangeSeekBar.getMinProgress();
        return rangeSeekBar.getMinProgress() + range * currPercent;
    }
}
