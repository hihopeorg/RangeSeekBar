/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jaygoo.widget;

public interface AttrConstant {


    String RSB_MAX = "rsb_max";
    String RSB_MIN = "rsb_min";
    String RSB_MODE = "rsb_mode";
    String RSB_GRAVITY = "rsb_gravity";
    String RSB_MIN_INTERVAL = "rsb_min_interval";
    String RSB_TICK_MARK_NUMBER = "rsb_tick_mark_number";
    String RSB_TICK_MARK_TEXT_ARRAY = "rsb_tick_mark_text_array";
    String RSB_TICK_MARK_TEXT_MARGIN = "rsb_tick_mark_text_margin";
    String RSB_TICK_MARK_TEXT_SIZE = "rsb_tick_mark_text_size";
    String RSB_TICK_MARK_TEXT_COLOR = "rsb_tick_mark_text_color";
    String RSB_TICK_MARK_MODE = "rsb_tick_mark_mode";
    String RSB_TICK_MARK_GRAVITY = "rsb_tick_mark_gravity";
    String RSB_TICK_MARK_LAYOUT_GRAVITY = "rsb_tick_mark_layout_gravity";
    String RSB_TICK_MARK_IN_RANGE_TEXT_COLOR = "rsb_tick_mark_in_range_text_color";
    String RSB_PROGRESS_HEIGHT= "rsb_progress_height";
    String RSB_PROGRESS_RADIUS = "rsb_progress_radius";
    String RSB_PROGRESS_COLOR = "rsb_progress_color";
    String RSB_PROGRESS_DEFAULT_COLOR = "rsb_progress_default_color";
    String RSB_PROGRESS_DRAWABLE = "rsb_progress_drawable";
    String RSB_PROGRESS_DRAWABLE_DEFAULT = "rsb_progress_drawable_default";
    String RSB_INDICATOR_SHOW_MODE = "rsb_indicator_show_mode";
    String RSB_INDICATOR_HEIGHT= "rsb_indicator_height";
    String RSB_INDICATOR_WEIGHT= "rsb_indicator_width";
    String RSB_INDICATOR_MARGIN= "rsb_indicator_margin";
    String RSB_INDICATOR_TEXT_SIZE= "rsb_indicator_text_size";
    String RSB_INDICATOR_TEXT_COLOR= "rsb_indicator_text_color";
    String RSB_INDICATOR_ARROW_SIZE= "rsb_indicator_arrow_size";
    String RSB_INDICATOR_DRAWABLE= "rsb_indicator_drawable";
    String RSB_INDICATOR_BACKGROUND_COLOR= "rsb_indicator_background_color";
    String RSB_INDICATOR_PADDING_LEFT= "rsb_indicator_padding_left";
    String RSB_INDICATOR_PADDING_RIGHT= "rsb_indicator_padding_right";
    String RSB_INDICATOR_PADDING_TOP= "rsb_indicator_padding_top";
    String RSB_INDICATOR_PADDING_BOTTOM= "rsb_indicator_padding_bottom";
    String RSB_INDICATOR_RADIUS= "rsb_indicator_radius";
    String RSB_THUMB_DRAWABLE= "rsb_thumb_drawable";
    String RSB_THUMB_WIDTH= "rsb_thumb_width";
    String RSB_THUMB_HEIGHT= "rsb_thumb_height";
    String RSB_THUMB_SCALE_RATIO= "rsb_thumb_scale_ratio";
    String RSB_THUMB_INACTIVATED_DRAWABLE= "rsb_thumb_inactivated_drawable";
    String RSB_STEPS = "rsb_steps";
    String RSB_STEPS_COLOR = "rsb_step_color";
    String RSB_STEPS_WIDTH = "rsb_step_width";
    String RSB_STEPS_HEIGHT = "rsb_step_height";
    String RSB_STEPS_RADIUS = "rsb_step_radius";
    String RSB_STEPS_AUTO_BONDING = "rsb_step_auto_bonding";
    String RSB_STEPS_DRAWABLE = "rsb_step_drawable";
    String RSB_ORIENTATION = "rsb_orientation";
    String RSB_TICK_MARK_ORIENTATION = "rsb_tick_mark_orientation";
    String RSB_INDICATOR_TEXT_ORIENTATION = "rsb_indicator_text_orientation";
}
