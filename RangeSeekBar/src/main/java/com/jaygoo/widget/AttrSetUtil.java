/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jaygoo.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.util.Locale;

public class AttrSetUtil {

    public static int getColor(AttrSet attrSet, String attrKey,Color colorValue) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getColorValue().getValue();
        }
        return colorValue.getValue();
    }

    public static Element getElement(AttrSet attrSet, String attrKey) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getElement();
        }
        return null;
    }

    public static int  getInt(AttrSet attrSet,String attrKey,int defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getIntegerValue();
        }
        return defaultValue;
    }

    public static String  getName(AttrSet attrSet,String attrKey,String defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getName();
        }
        return defaultValue;
    }
    public static float  getFloat(AttrSet attrSet,String attrKey,float defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getFloatValue();
        }
        return defaultValue;
    }

    public static boolean  getBoolean(AttrSet attrSet,String attrKey,boolean defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getBoolValue();
        }
        return defaultValue;
    }
    public static String  getString(AttrSet attrSet,String attrKey,String defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getStringValue();
        }
        return defaultValue;
    }

    public static float  getDimension(AttrSet attrSet,String attrKey,float defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getDimensionValue();
        }
        return defaultValue;
    }

    public static String[]  getStringArray(Context context,int resId){
      return  context.getStringArray(resId);
    }

    public static int getResourceId(String resource){
        if (TextTool.isNullOrEmpty(resource)) return 0;
        String[] content = resource.split(":");
        if (content!=null && content.length>1){
            return Integer.parseInt(content[1]);
        }
        return 0;
    }

    /**
     * invoke the resId of app bundle
     *
     * @param context      for get  package name
     * @param referenceStr for example {$media:i0_selected}
     * @return resource id
     */
    public static int invokeResId(Context context, String referenceStr) {
        String[] split = referenceStr.split(":");
        String type = split[0].substring(1);
        String fieldName = type.substring(0, 1).toUpperCase(Locale.ROOT) + type.substring(1) + "_" + split[1];
        try {
            Class<?> aClass = Class.forName(context.getBundleName() + ".ResourceTable");
            Field field = aClass.getDeclaredField(fieldName);
            return field.getInt(null);
        } catch (Exception e) {
            System.out.println("invokeResId is exception:"+e.getMessage());
        }
        return -1;
    }
}
