/**
 * Copyright (c) Huawei Technologies Co., Ltd.
 */

package com.jaygoo.widget;

public enum ShowMode {
    TOUCH(0),HIDE(1),AFTERTOUCH(2),SHOW(3);
    int value;

    ShowMode(int value) {
        this.value= value;
    }
}
