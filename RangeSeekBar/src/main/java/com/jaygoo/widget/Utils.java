package com.jaygoo.widget;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.*;

import java.io.IOException;
import java.util.Optional;

/**
 * ================================================
 * 作    者：JayGoo
 * 版    本：
 * 创建日期：2018/5/8
 * 描    述:
 * ================================================
 */
public class Utils {

    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000100, "RangeSeekBar");
    private static final String TAG = "RangeSeekBar";

    public static void print(String log) {

    }

    public static void print(Object... logs) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object log : logs) {
            stringBuilder.append(log);
        }
        HiLog.info(label, "util print:%{public}s", stringBuilder.toString());
    }

    public static PixelMap drawableToBitmap(Context context, int width, int height, int drawableId) {
        if (context == null || width <= 0 || height <= 0 || drawableId == 0) return null;
        PixelMap pixelMap = Utils.drawableToBitmap(width, height, ElementScatter.getInstance(context).parse(drawableId));
        return pixelMap;
    }

    /**
     * make a drawable to a bitmap
     *
     * @param drawable drawable you want convert
     * @return converted bitmap
     */
    public static PixelMap drawableToBitmap(int width, int height, Element drawable) {
        PixelMap bitmap = null;
        try {
            if (drawable instanceof PixelMapElement) {
                PixelMapElement bitmapDrawable = (PixelMapElement) drawable;
                bitmap = bitmapDrawable.getPixelMap();
                int bitmapHeight = bitmap.getImageInfo().size.height;
                int bitmapWidth = bitmap.getImageInfo().size.width;
                if (bitmap != null && bitmapHeight > 0) {
                    Matrix matrix = new Matrix();
                    float scaleWidth = width * 1.0f / bitmapWidth;
                    float scaleHeight = height * 1.0f / bitmapHeight;
                    matrix.postScale(scaleWidth, scaleHeight);
                    bitmap = createPixelMap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
                    return bitmap;
                }
            }
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            bitmap = PixelMap.create(options);
            Texture texture = new Texture(bitmap);
            Canvas canvas = new Canvas(texture);
            drawable.setBounds(0, 0, texture.getWidth(), texture.getHeight());
            drawable.drawToCanvas(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static PixelMap drawableToBitmap(int width, int height, PixelMapElement drawable) {
        PixelMap bitmap = null;
        try {
            PixelMapElement bitmapDrawable = (PixelMapElement) drawable;
            bitmap = bitmapDrawable.getPixelMap();
            int bitmapHeight = bitmap.getImageInfo().size.height;
            int bitmapWidth = bitmap.getImageInfo().size.width;
            if (bitmap != null && bitmapHeight > 0) {
                Matrix matrix = new Matrix();
                float scaleWidth = width * 1.0f / bitmapWidth;
                float scaleHeight = height * 1.0f / bitmapHeight;
                matrix.postScale(scaleWidth, scaleHeight);
                bitmap = createPixelMap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static PixelMap createPixelMap(PixelMap source, int x, int y, int width, int height,
                                          Matrix matrix, boolean filter) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();

        RectFloat srcR = new RectFloat(x, y, x + width, y + height);
        RectFloat dstR = new RectFloat(0, 0, width, height);
        RectFloat deviceR = new RectFloat();
        Paint paint = new Paint();
        paint.setFilterBitmap(filter);
        final boolean transformed = !matrix.rectStaysRect();
        if (transformed) {
            paint.setAntiAlias(true);
        }
        matrix.mapRect(deviceR, dstR);

        int neww = Math.round(deviceR.getWidth());
        int newh = Math.round(deviceR.getHeight());
        initializationOptions.size = new Size(neww, newh);
        initializationOptions.scaleMode = ScaleMode.CENTER_CROP;
        initializationOptions.alphaType = AlphaType.OPAQUE;
        PixelMap srcPixelMap = PixelMap.create(initializationOptions);
        Texture texture = new Texture(srcPixelMap);
        Canvas canvas = new Canvas(texture);

        canvas.translate(-deviceR.left, -deviceR.top);
        canvas.concat(matrix);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(source), srcR, dstR, paint);
        return texture.getPixelMap();
    }

//    /**
//     * draw 9Path
//     *
//     * @param canvas Canvas
//     * @param bmp    9path bitmap
//     * @param rect   9path rect
//     */
//    public static void drawNinePath(Canvas canvas, PixelMap bmp, RectFloat rect) {
//        NinePatch.isNinePatchChunk(bmp.getNinePatchChunk());
//        NinePatch patch = new NinePatch(bmp, bmp.getNinePatchChunk(), null);
//        patch.draw(canvas, rect);
//    }

    public static void drawBitmap(Canvas canvas, Paint paint, PixelMap bmp, RectFloat rect) {
        canvas.drawPixelMapHolder(new PixelMapHolder(bmp), rect.left, rect.top, paint);
    }

    public static int dp2px(Context context, float dpValue) {
        if (context == null || compareFloat(0f, dpValue) == 0) return 0;
        Display displayManager = DisplayManager.getInstance().getDefaultDisplay(context).get();

        final float scale = displayManager.getRealAttributes().densityDpi;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * Compare the size of two floating point numbers
     *
     * @param a
     * @param b
     * @return 1 is a > b
     * -1 is a < b
     * 0 is a == b
     */
    public static int compareFloat(float a, float b) {
        int ta = Math.round(a * 1000000);
        int tb = Math.round(b * 1000000);
        if (ta > tb) {
            return 1;
        } else if (ta < tb) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Compare the size of two floating point numbers with accuracy
     *
     * @param a
     * @param b
     * @return 1 is a > b
     * -1 is a < b
     * 0 is a == b
     */
    public static int compareFloat(float a, float b, int degree) {
        if (Math.abs(a - b) < Math.pow(0.1, degree)) {
            return 0;
        } else {
            if (a < b) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static float parseFloat(String s) {
        try {
            return Float.parseFloat(s);
        } catch (NumberFormatException e) {
            return 0f;
        }
    }

    public static Rect measureText(String text, float textSize) {
        Paint paint = new Paint();
        Rect textRect;
        paint.setTextSize((int) textSize);
        textRect = paint.getTextBounds(text);
        paint.reset();
        return textRect;
    }

    public static boolean verifyBitmap(PixelMap bitmap) {
        if (bitmap == null || bitmap.isReleased()) {
            return false;
        }
        ImageInfo info = bitmap.getImageInfo();
        if (info == null) {
            return false;
        }
        Size size = info.size;
        if (size.width <= 0 || size.height <= 0) {
            return false;
        }
        return true;
    }

    public static int getColor(Context context, int colorId) {
        if (context != null) {
            return context.getColor(colorId);
        }
        return Color.WHITE.getValue();
    }

    public static PixelMap decodeResource(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            if (path.isEmpty()) {
                return null;
            }
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
