# RangeSeekBar

**本项目是基于开源项目RangeSeekBar进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Jay-Goo/RangeSeekBar ）追踪到原项目版本**

#### 项目介绍

- 项目名称：RangeSeekBar
- 所属系列：ohos的第三方组件适配移植
- 功能： 一款美观强大的支持单向、双向范围选择、分步、垂直、高度自定义的SeekBar
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/Jay-Goo/RangeSeekBar
- 原项目基线版本：V2.0.4
- 编程语言：Java
- 外部库依赖：无

#### 效果演示

<img src="screenshot/效果动画.gif"/>

#### 安装教程

方案一：

1. 编译har包RangeSeekBar.har。
2. 启动 DevEco Studio，将har包导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'RangeSeekBar', ext: 'har')
	……
}
```

方案二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

   ```
   repositories {
       maven {
           url 'http://106.15.92.248:8081/repository/Releases/' 
       }
   }
   ```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

   ```
   dependencies {
       implementation 'com.jaygoo.widget:rangeseekbar:1.0.1'
   }
   ```

   

#### 使用说明

1.单个进度条按钮类型：

  1.1：常规进度条按钮：
  ```xml
    <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_single_1"
        ohos:width="match_parent"
        ohos:height="30vp"
        ohos:top_margin="10vp"
        app:rsb_gravity="center"
        app:rsb_mode="single"
        app:rsb_thumb_drawable="$graphic:thumb_green_alpha"/>
  ```
   1.2:按钮带点击效果
   ```xml
    <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_single_3"
        ohos:width="match_parent"
        ohos:top_margin="10vp"
        ohos:height="30vp"
        app:rsb_gravity="center"
        app:rsb_mode="single"
        app:rsb_thumb_drawable="$graphic:thumb_activated_default"
        app:rsb_thumb_height="26vp"
        app:rsb_thumb_inactivated_drawable="$graphic:thumb_activated"
        app:rsb_thumb_scale_ratio="1.4"
        app:rsb_thumb_width="26vp"/>
   ```
   1.3:进度按钮为图片且显示进度值
   ```xml
  <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_single_6"
        ohos:width="match_parent"
        ohos:top_margin="10vp"
        ohos:height="match_content"
        app:rsb_gravity="bottom"
        app:rsb_indicator_background_color="$color:colorRed"
        app:rsb_indicator_height="30vp"
        app:rsb_indicator_padding_bottom="10vp"
        app:rsb_indicator_padding_left="5vp"
        app:rsb_indicator_padding_right="5vp"
        app:rsb_indicator_padding_top="10vp"
        app:rsb_indicator_radius="5vp"
        app:rsb_indicator_show_mode="alwaysShow"
        app:rsb_indicator_text_size="12fp"
        app:rsb_indicator_width="50vp"
        app:rsb_mode="single"
        app:rsb_progress_color="$color:colorRed"
        app:rsb_progress_default_color="$color:rsbColorSeekBarDefault"
        app:rsb_thumb_drawable="$media:thumb_driver"
        app:rsb_thumb_height="25vp"
        app:rsb_thumb_width="30vp"/>
   ```
 1.4:进度状态配置
 ```xml
    <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_single_7"
        ohos:width="match_parent"
        ohos:top_margin="10vp"
        ohos:height="match_content"
        ohos:left_padding="10vp"
        app:rsb_gravity="bottom"
        app:rsb_indicator_drawable="$media:indicator_cloud"
        app:rsb_indicator_height="46vp"
        app:rsb_indicator_padding_bottom="10vp"
        app:rsb_indicator_padding_left="10vp"
        app:rsb_indicator_padding_right="10vp"
        app:rsb_indicator_padding_top="10vp"
        app:rsb_indicator_show_mode="alwaysShowAfterTouch"
        app:rsb_indicator_text_color="#aaa"
        app:rsb_indicator_text_size="12fp"
        app:rsb_indicator_width="50vp"
        app:rsb_mode="single"
        app:rsb_progress_color="$color:colorRed"
        app:rsb_progress_default_color="$color:rsbColorSeekBarDefault"
        app:rsb_thumb_drawable="$media:thumb_driver"
        app:rsb_tick_mark_layout_gravity="top"
        app:rsb_tick_mark_mode="other"
        app:rsb_tick_mark_text_color="$color:colorRed"
        app:rsb_tick_mark_text_margin="15vp"
        app:rsb_thumb_height="25vp"
        app:rsb_thumb_width="30vp"/>
 ```
 由于ohos无法从xml引入strArray，需要java api调用：
 ```java
        RangeSeekBar rangeSeekBar7= (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);

        String[] distribution =null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_markArray3).getStringArray();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        rangeSeekBar7.setTickMarkTextArray(distribution);
 ```
2.多个进度条按钮：
```xml
    <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_range_3"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:top_margin="10vp"
        app:rsb_mode="range"
        app:rsb_tick_mark_layout_gravity="bottom"
        app:rsb_tick_mark_text_margin="15vp"
        app:rsb_progress_color="$color:colorProgress"
        app:rsb_min_interval="50"
        app:rsb_thumb_drawable="$graphic:thumb_yellow_stroke"
        app:rsb_indicator_show_mode="alwaysShow"
        app:rsb_indicator_height="30vp"
        app:rsb_indicator_width="50vp"
        app:rsb_indicator_margin="5vp"
        app:rsb_indicator_background_color="$color:colorProgress"
        app:rsb_tick_mark_mode="number"
        app:rsb_tick_mark_text_array="@array/markArray5"/>
```
3.步骤式进度条
```xml
    <com.jaygoo.widget.RangeSeekBar
        ohos:id="$+id:sb_step_1"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:top_margin="20vp"
        app:rsb_gravity="center"
        app:rsb_mode="single"
        app:rsb_progress_drawable="$graphic:progress"
        app:rsb_progress_drawable_default="$graphic:progress_defalut"
        app:rsb_step_auto_bonding="true"
        app:rsb_step_height="23vp"
        app:rsb_step_width="23vp"
        app:rsb_steps="3"
        app:rsb_thumb_drawable="$media:thumb_ghost"
        app:rsb_thumb_height="31vp"
        app:rsb_thumb_width="25vp" />
```
4.垂直方向进度条
```xml
       <com.jaygoo.widget.VerticalRangeSeekBar
            ohos:id="$+id:sb_vertical_3"
            ohos:width="match_content"
            ohos:height="match_parent"
            ohos:left_margin="30vp"
            app:rsb_gravity="bottom"
            app:rsb_indicator_height="30vp"
            app:rsb_indicator_padding_bottom="5vp"
            app:rsb_indicator_padding_left="10vp"
            app:rsb_indicator_padding_right="10vp"
            app:rsb_indicator_padding_top="5vp"
            app:rsb_indicator_show_mode="alwaysShow"
            app:rsb_indicator_width="30vp"
            app:rsb_mode="single"
            app:rsb_orientation="left"
            app:rsb_progress_color="$color:colorAccent"
            app:rsb_progress_default_color="$color:rsbColorSeekBarDefault"
            app:rsb_thumb_drawable="$media:thumb_default"
            app:rsb_tick_mark_mode="number"
            app:rsb_tick_mark_text_array="@array/markArray"/>
```
#### 版本迭代


- v1.0.1


#### 版权和许可信息
Apache License, Version 2.0 