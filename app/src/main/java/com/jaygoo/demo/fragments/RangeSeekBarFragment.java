package com.jaygoo.demo.fragments;

import com.jaygoo.demo.ResourceTable;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.jaygoo.widget.SeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class RangeSeekBarFragment extends BaseFragment {

    private Component  component;

    @Override
    public Component getRootComponent(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_fragment_range,container,false);
        handleRange(component);
        return component;
    }

    @Override
    public Component getRootComponent() {
        return component;
    }

    private  void handleRange(Component component) {
        RangeSeekBar sb_range_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_1);
        RangeSeekBar sb_range_2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_2);
        RangeSeekBar sb_range_3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_3);
        RangeSeekBar sb_range_4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_4);
        RangeSeekBar sb_range_5 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_5);
        RangeSeekBar sb_range_6 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_6);
        sb_range_1.setProgress(0f, 100f);
        changeSeekBarThumb(component.getContext(),sb_range_1.getLeftSeekBar(), sb_range_1.getLeftSeekBar().getProgress());
        changeSeekBarThumb(component.getContext(),sb_range_1.getRightSeekBar(), sb_range_1.getRightSeekBar().getProgress());
        sb_range_1.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar rangeSeekBar, float leftValue, float rightValue, boolean isFromUser) {
                changeSeekBarThumb(component.getContext(),rangeSeekBar.getLeftSeekBar(), leftValue);
                changeSeekBarThumb(component.getContext(),rangeSeekBar.getRightSeekBar(), rightValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });
        sb_range_2.setProgress(0f, 100f);

        sb_range_3.setRange(-100f, 100f);
        sb_range_3.setProgress(0f, 80f);
        sb_range_3.setIndicatorTextDecimalFormat("0");

        sb_range_4.setProgress(20f, 70f);

        sb_range_5.setProgress(20f, 60f);
        String[] distribution =null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_wordsArray).getStringArray();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        sb_range_5.setTickMarkTextArray(distribution);

        sb_range_6.setProgress(20f, 60f);
        Element defaultPixelMapElement= null;
        Element defaultPixelMapElement2= null;
        try {
            Resource resource = component.getContext().getResourceManager().getResource(ResourceTable.Media_step_1);
            Resource resource2 = component.getContext().getResourceManager().getResource(ResourceTable.Media_step_2);
            defaultPixelMapElement = new PixelMapElement(resource);
            defaultPixelMapElement2 = new PixelMapElement(resource2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        sb_range_6.getLeftSeekBar().setThumbDrawableId(defaultPixelMapElement);
        sb_range_6.getRightSeekBar().setThumbDrawableId(defaultPixelMapElement2);
    }


    private void  changeSeekBarThumb(Context context,SeekBar seekBar, float value){
        if (value < 33){
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_green),seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }else if (value < 66){
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_yellow), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }else{
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_red), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }
    }

}
