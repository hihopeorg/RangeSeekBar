package com.jaygoo.demo.fragments;

import com.jaygoo.demo.ResourceTable;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.jaygoo.widget.SeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class VerticalSeekBarFragment extends BaseFragment {
    private Component  component;

    @Override
    public Component getRootComponent(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_fragment_vertical,container,false);
        handleVertical(component);
        return component;
    }

    @Override
    public Component getRootComponent() {
        return component;
    }

    private void handleVertical(Component component) {
        RangeSeekBar rb_vertical_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_1);
        RangeSeekBar rb_vertical_2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_2);
        RangeSeekBar rb_vertical_3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_3);
        RangeSeekBar rb_vertical_4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_4);
        rb_vertical_2.setIndicatorTextDecimalFormat("0.0");
        rb_vertical_2.setProgress(0f, 100f);
        changeSeekBarThumb(component.getContext(),rb_vertical_2.getLeftSeekBar(), rb_vertical_2.getLeftSeekBar().getProgress());
        changeSeekBarThumb(component.getContext(),rb_vertical_2.getRightSeekBar(), rb_vertical_2.getRightSeekBar().getProgress());
        rb_vertical_2.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar rangeSeekBar, float leftValue, float rightValue, boolean isFromUser) {
                changeSeekBarThumb(component.getContext(),rangeSeekBar.getLeftSeekBar(), leftValue);
                changeSeekBarThumb(component.getContext(),rangeSeekBar.getRightSeekBar(), rightValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });

        rb_vertical_3.setIndicatorTextDecimalFormat("0");
        String[] distribution =null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_markArray).getStringArray();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

        rb_vertical_3.setTickMarkTextArray(distribution);
        rb_vertical_4.setTickMarkTextArray(distribution);
    }
    private void  changeSeekBarThumb(Context context,SeekBar seekBar, float value){
        if (value < 33){
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_green),seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }else if (value < 66){
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_yellow), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }else{
            seekBar.setThumbDrawableId(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_thumb_red), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }
    }
}
