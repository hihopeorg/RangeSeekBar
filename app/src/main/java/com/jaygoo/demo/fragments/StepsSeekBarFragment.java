package com.jaygoo.demo.fragments;

import com.jaygoo.demo.ResourceTable;
import com.jaygoo.widget.RangeSeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StepsSeekBarFragment extends BaseFragment {

    private Component  component;

    @Override
    public Component getRootComponent(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_fragment_step,container,false);
        handleStep(component);
        return component;
    }

    @Override
    public Component getRootComponent() {
        return component;
    }

    private void handleStep(Component component) {
        RangeSeekBar sb_steps_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_1);
        RangeSeekBar sb_steps_5 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_5);
        List<Integer> stepsDrawables = new ArrayList<>();
        stepsDrawables.add(ResourceTable.Media_step_1);
        stepsDrawables.add(ResourceTable.Media_step_2);
        stepsDrawables.add(ResourceTable.Media_step_3);
        stepsDrawables.add(ResourceTable.Media_step_4);
        sb_steps_1.setStepsDrawable(stepsDrawables);
        String[] distribution =null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_wordsArray).getStringArray();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        sb_steps_5.setTickMarkTextArray(distribution);

    }
}
