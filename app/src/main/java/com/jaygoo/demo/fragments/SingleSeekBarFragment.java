package com.jaygoo.demo.fragments;

import com.jaygoo.demo.ResourceTable;
import com.jaygoo.widget.RangeSeekBar;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;


public class SingleSeekBarFragment  extends BaseFragment {
    private Component  component;

    @Override
    public Component getRootComponent(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_fragment_single,container,false);
        handleSingle(component);
        return component;
    }

    @Override
    public Component getRootComponent() {
        return component;
    }

    private void handleSingle(Component component) {
        RangeSeekBar rangeSeekBar7= (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);

        String[] distribution =null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_markArray3).getStringArray();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        rangeSeekBar7.setTickMarkTextArray(distribution);
    }
}
