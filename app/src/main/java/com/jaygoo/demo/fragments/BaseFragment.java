package com.jaygoo.demo.fragments;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public abstract class BaseFragment extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return getRootComponent(scatter, container, intent);
    }

    protected abstract Component  getRootComponent(LayoutScatter scatter, ComponentContainer container, Intent intent);
    protected abstract Component  getRootComponent();
}
