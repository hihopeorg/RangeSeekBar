
package com.jaygoo.demo;

import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.jaygoo.widget.SeekBar;
import com.jaygoo.widget.Utils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.TabList;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability {
    public StackLayout fractionContainer;
    public TabList tabList;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initFragment();
    }

    private void initFragment() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tabList);
        fractionContainer = (StackLayout) findComponentById(ResourceTable.Id_fraction_container);
        TabList.Tab tab1 = tabList.new Tab(this);
        tab1.setText("SINGLE");
        TabList.Tab tab2 = tabList.new Tab(this);
        tab2.setText("RANGE");
        TabList.Tab tab3 = tabList.new Tab(this);
        tab3.setText("STEPS");
        TabList.Tab tab4 = tabList.new Tab(this);
        tab4.setText("VERTICAL");
        tabList.addTab(tab1);
        tabList.addTab(tab2);
        tabList.addTab(tab3);
        tabList.addTab(tab4);
        tabList.selectTabAt(0);
        Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_single, null, false);
        fractionContainer.removeAllComponents();
        fractionContainer.addComponent(component);
        handleSingle(component);
        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                // 当某个Tab从未选中状态变为选中状态时的回调
                if (fractionContainer.getChildCount() > 0) {
                    fractionContainer.removeAllComponents();
                }
                int i = tab.getPosition();
                Component component = null;
                if (i == 0) {
                    component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_single, null, false);
                    handleSingle(component);
                } else if (i == 1) {
                    component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_range, null, false);
                    handleRange(component);
                } else if (i == 2) {
                    component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_step, null, false);
                    handleStep(component);
                } else {
                    component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_vertical, null, false);
                    handleVertical(component);
                }
                fractionContainer.addComponent(component);
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
                // 当某个Tab从选中状态变为未选中状态时的回调
            }

            @Override
            public void onReselected(TabList.Tab tab) {
                // 当某个Tab已处于选中状态，再次被点击时的状态回调
            }
        });
    }

    private StackLayout getRootComponent() {
        return fractionContainer;
    }

    private void handleSingle(Component component) {
        RangeSeekBar sb_single8 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_8);
        RangeSeekBar sb_single7 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);
        RangeSeekBar sb_single1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_1);
        RangeSeekBar sb_single2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_2);
        RangeSeekBar sb_single3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_3);
        RangeSeekBar sb_single4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_4);
        sb_single1.setProgress(10f);
        sb_single2.setProgress(20f);
        sb_single3.setProgress(30f);
        sb_single4.setProgress(40f);
        String[] distribution = null;
        try {
            distribution = component.getContext().getResourceManager().getElement(ResourceTable.Strarray_markArray3).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        sb_single7.setTickMarkTextArray(distribution);
        sb_single8.setTickMarkTextArray(distribution);
        sb_single7.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar rangeSeekBar, float leftValue, float rightValue, boolean isFromUser) {
                if (leftValue <= 33.33) {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("赶往店中");
                } else if (leftValue > 33.33 && leftValue <= 66.66) {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("制作中");
                } else if (leftValue > 66.66 && leftValue < 100) {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("配送中");
                } else {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("已送达");
                }
                if (rangeSeekBar.getRangeSeekBarState()[0].isMin) {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("商家接单");
                } else if (rangeSeekBar.getRangeSeekBarState()[0].isMax) {
                    rangeSeekBar.getLeftSeekBar().setIndicatorText("已送达");
                }
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }
        });
    }

    private void handleRange(Component component) {
        RangeSeekBar sb_range_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_1);
        RangeSeekBar sb_range_2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_2);
        RangeSeekBar sb_range_3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_3);
        RangeSeekBar sb_range_4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_4);
        RangeSeekBar sb_range_5 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_5);
        RangeSeekBar sb_range_6 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_6);
        sb_range_1.setProgress(0f, 100f);
        changeSeekBarThumb(sb_range_1.getLeftSeekBar(), sb_range_1.getLeftSeekBar().getProgress());
        changeSeekBarThumb(sb_range_1.getRightSeekBar(), sb_range_1.getRightSeekBar().getProgress());
        sb_range_1.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar rangeSeekBar, float leftValue, float rightValue, boolean isFromUser) {
                changeSeekBarThumb(rangeSeekBar.getLeftSeekBar(), leftValue);
                changeSeekBarThumb(rangeSeekBar.getRightSeekBar(), rightValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }
        });
        sb_range_2.setProgress(0f, 100f);
        sb_range_3.setRange(-100f, 100f);
        sb_range_3.setProgress(0f, 80f);
        sb_range_3.setIndicatorTextDecimalFormat("0");
        sb_range_4.setRange(0, 100);
        sb_range_4.setProgress(20f, 70f);
        String[] distribution = null;
        try {
            distribution = getResourceManager().getElement(ResourceTable.Strarray_wordsArray).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        sb_range_5.setTickMarkTextArray(distribution);
        sb_range_5.setProgress(20f, 70f);
        sb_range_6.setProgress(20f, 60f);
        Element defaultPixelMapElement = null;
        Element defaultPixelMapElement2 = null;
        try {
            Resource resource = component.getContext().getResourceManager().getResource(ResourceTable.Media_step_1);
            Resource resource2 = component.getContext().getResourceManager().getResource(ResourceTable.Media_step_2);
            defaultPixelMapElement = new PixelMapElement(resource);
            defaultPixelMapElement2 = new PixelMapElement(resource2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        sb_range_6.getLeftSeekBar().setThumbDrawableId(defaultPixelMapElement);
        sb_range_6.getRightSeekBar().setThumbDrawableId(defaultPixelMapElement2);
    }

    private void changeSeekBarThumb(SeekBar seekBar, float value) {
        if (value < 33) {
            seekBar.setThumbDrawableId(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_thumb_green), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        } else if (value < 66) {
            seekBar.setThumbDrawableId(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_thumb_yellow), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        } else {
            seekBar.setThumbDrawableId(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_thumb_red), seekBar.getThumbWidth(), seekBar.getThumbHeight());
        }
    }

    private void handleStep(Component component) {
        RangeSeekBar sb_steps_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_1);
        RangeSeekBar sb_steps_2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_2);
        RangeSeekBar sb_steps_5 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_5);
        RangeSeekBar sb_steps_6 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_6);
        RangeSeekBar sb_steps_7 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_7);
        List<Integer> stepsDrawables = new ArrayList<>();
        stepsDrawables.add(ResourceTable.Media_step_1);
        stepsDrawables.add(ResourceTable.Media_step_2);
        stepsDrawables.add(ResourceTable.Media_step_3);
        stepsDrawables.add(ResourceTable.Media_step_4);
        sb_steps_1.setStepsDrawable(stepsDrawables);
        sb_steps_2.setStepsDrawable(stepsDrawables);
        String[] distribution = null;
        String[] mark = null;
        try {
            distribution = getResourceManager().getElement(ResourceTable.Strarray_wordsArray).getStringArray();
            mark = getResourceManager().getElement(ResourceTable.Strarray_markArray).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        sb_steps_5.setTickMarkTextArray(distribution);
        sb_steps_6.setTickMarkTextArray(distribution);
        sb_steps_7.setTickMarkTextArray(mark);
    }

    private void handleVertical(Component component) {
        RangeSeekBar rb_vertical_1 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_1);
        RangeSeekBar rb_vertical_2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_2);
        RangeSeekBar rb_vertical_3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_3);
        RangeSeekBar rb_vertical_4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_4);
        RangeSeekBar rb_vertical_7 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_7);
        rb_vertical_2.setIndicatorTextDecimalFormat("0.0");
        rb_vertical_2.setProgress(0f, 100f);
        changeSeekBarThumb(rb_vertical_2.getLeftSeekBar(), rb_vertical_2.getLeftSeekBar().getProgress());
        changeSeekBarThumb(rb_vertical_2.getRightSeekBar(), rb_vertical_2.getRightSeekBar().getProgress());
        rb_vertical_2.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar rangeSeekBar, float leftValue, float rightValue, boolean isFromUser) {
                changeSeekBarThumb(rangeSeekBar.getLeftSeekBar(), leftValue);
                changeSeekBarThumb(rangeSeekBar.getRightSeekBar(), rightValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }
        });
        rb_vertical_3.setIndicatorTextDecimalFormat("0");
        String[] distribution = null;
        try {
            distribution = getResourceManager().getElement(ResourceTable.Strarray_markArray).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        rb_vertical_3.setTickMarkTextArray(distribution);
        rb_vertical_4.setTickMarkTextArray(distribution);
        List<Integer> stepsDrawables = new ArrayList<>();
        stepsDrawables.add(ResourceTable.Media_step_1);
        stepsDrawables.add(ResourceTable.Media_step_2);
        stepsDrawables.add(ResourceTable.Media_step_3);
        stepsDrawables.add(ResourceTable.Media_step_4);
        rb_vertical_7.setStepsDrawable(stepsDrawables);
        rb_vertical_7.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                changeSeekBarIndicator(view.getLeftSeekBar(), leftValue);
                changeSeekBarIndicator(view.getRightSeekBar(), rightValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {
            }
        });
    }

    private void changeSeekBarIndicator(SeekBar seekbar, Float value) {
        seekbar.showIndicator(true);
        if (Utils.compareFloat(value, 0f, 3) == 0 || Utils.compareFloat(value, 100f, 3) == 0) {
            seekbar.setIndicatorText("smile");
        } else if (Utils.compareFloat(value, 100 / 3f, 3) == 0) {
            seekbar.setIndicatorText("naughty");
        } else if (Utils.compareFloat(value, 200 / 3f, 3) == 0) {
            seekbar.setIndicatorText("lovely");
        } else {
            seekbar.showIndicator(false);
        }
    }

    public void resetSelected(int position) {
        tabList.selectTabAt(position);
    }
}
