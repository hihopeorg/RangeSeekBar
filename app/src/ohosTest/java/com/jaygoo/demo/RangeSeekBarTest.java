/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.jaygoo.demo;


import com.jaygoo.widget.RangeSeekBar;
import com.jaygoo.widget.SeekBar;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * 功能性测试
 */
public class RangeSeekBarTest extends TestCase {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private MainAbility ability;
    private ShapeElement element;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testAbility() {
        assertNotNull("testAbility failed, can not start ability", ability);
    }

    @Test
    public void testSeekBarProgress() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_1);
        assertNotNull("test rangeSeekBar", rangeSeekBar);
        SeekBar seekBar = rangeSeekBar.getLeftSeekBar();
        assertNotNull("test seekBar", seekBar);
        assertEquals("testSeekBarProgress", 10.0f, seekBar.getProgress());
    }

    @Test
    public void testTickMarkTextArray() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);
        assertNotNull("test rangeSeekBar", rangeSeekBar);
        String[] markTexts = rangeSeekBar.getTickMarkTextArray();
        assertEquals("testSeekBarProgress", "商家接单", markTexts[0]);
    }

    @Test
    public void testThumbDrawable() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_6);
        assertNotNull("test rangeSeekBar", rangeSeekBar);
        SeekBar seekBar = rangeSeekBar.getLeftSeekBar();
        assertNotNull("test seekBar", seekBar);
        assertNotNull("test seekBarDrawable", seekBar.getThumbDrawableId());
    }

    @Test
    public void testRangeSeekBar() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_5);
        assertNotNull("testRangeSeekBarMode", rangeSeekBar);
        SeekBar seekBar = rangeSeekBar.getLeftSeekBar();
        assertNotNull("testRangeSeekBar", seekBar);
        PixelMapElement element = (PixelMapElement) seekBar.getThumbDrawableId();
        assertNotNull("testRangeSeekBar", element);
        assertEquals("testRangeSeekBarModer", 75, element.getPixelMap().getImageInfo().size.height);
    }

    @Test
    public void testRangeSeekBarMode() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_6);
        assertNotNull("testRangeSeekBarMode", rangeSeekBar);
        assertEquals("testRangeSeekBarModer", RangeSeekBar.SEEKBAR_MODE_SINGLE, rangeSeekBar.getSeekBarMode());
    }

    @Test
    public void testIndicatorShowMode() {
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);
        SeekBar seekBar = rangeSeekBar.getLeftSeekBar();
        assertNotNull("testRangeSeekBarMode", seekBar);
        assertEquals("testRangeSeekBarMode", seekBar.INDICATOR_ALWAYS_SHOW_AFTER_TOUCH, seekBar.getIndicatorShowMode());
    }
}