/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.jaygoo.demo;

import com.jaygoo.widget.RangeSeekBar;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.TabList;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * UI 自动化
 */
public class ExampleOhosTest {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private MainAbility ability;

    @Before
    public void obtainAbility() {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testAbility() {
        assertNotNull("testAbility failed, can not start ability", ability);
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.jaygoo.demo", actualBundleName);
    }


    @Test
    public void testSingleSeekBar() {
        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tabList);
        iAbilityDelegator.runOnUIThreadSync(() -> tabList.selectTabAt(0));
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_1);
        AtomicInteger width = new AtomicInteger();
        iAbilityDelegator.runOnUIThreadSync(() ->
                width.set(rangeSeekBar.getWidth())
        );
        EventHelper.inputSwipe(ability, rangeSeekBar, (int) (width.get() * 0.1f), 60, (int) (width.get() * 0.8f), 60, 2000);
        assertNotNull(rangeSeekBar.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar2 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_2);
        EventHelper.inputSwipe(ability, rangeSeekBar2, (int) (width.get() * 0.1f), 150, (int) (width.get() * 0.8f), 150, 2000);
        assertNotNull(rangeSeekBar2.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar2.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar3 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_3);
        EventHelper.inputSwipe(ability, rangeSeekBar3, (int) (width.get() * 0.3f), 300, (int) (width.get() * 0.8f), 300, 2000);
        assertNotNull(rangeSeekBar3.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar3.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar4 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_4);
        EventHelper.inputSwipe(ability, rangeSeekBar4, (int) (width.get() * 0.3f), 420, (int) (width.get() * 0.8f), 420, 2000);
        assertNotNull(rangeSeekBar4.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar4.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar5 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_5);
        EventHelper.inputSwipe(ability, rangeSeekBar5, (int) (width.get() * 0.0f), 660, (int) (width.get() * 0.8f), 660, 2000);
        assertNotNull(rangeSeekBar5.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar5.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar6 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_6);
        EventHelper.inputSwipe(ability, rangeSeekBar6, (int) (width.get() * 0.0f), 900, (int) (width.get() * 0.8f), 900, 2000);
        assertNotNull(rangeSeekBar6.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar6.getLeftSeekBar().getProgress(), 3000);

        RangeSeekBar rangeSeekBar7 = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_single_7);
        EventHelper.inputSwipe(ability, rangeSeekBar7, (int) (width.get() * 0.0f), 1290, (int) (width.get() * 0.8f), 1290, 2000);
        assertNotNull(rangeSeekBar7.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar7.getLeftSeekBar().getProgress(), 3000);

    }

    @Test
    public void testRangeSeekBar() {

        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tabList);
        iAbilityDelegator.runOnUIThreadSync(() -> tabList.selectTabAt(1));
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_range_1);
        AtomicInteger width = new AtomicInteger();
        iAbilityDelegator.runOnUIThreadSync(() ->
                width.set(rangeSeekBar.getWidth())
        );
        EventHelper.inputSwipe(ability, rangeSeekBar, (int) (width.get() * 0.0f), 60, (int) (width.get() * 0.7f), 60, 2000);
        assertNotNull(rangeSeekBar.getLeftSeekBar());
        assertEquals(69, (int) rangeSeekBar.getLeftSeekBar().getProgress(), 3000);

    }

    @Test
    public void testStepsSeekBar() {
        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tabList);
        iAbilityDelegator.runOnUIThreadSync(() -> tabList.selectTabAt(2));
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_step_1);
        AtomicInteger width = new AtomicInteger();
        iAbilityDelegator.runOnUIThreadSync(() ->
                width.set(rangeSeekBar.getWidth())
        );
        EventHelper.inputSwipe(ability, rangeSeekBar, (int) (width.get() * 0.0f), 60, (int) (width.get() * 0.8f), 60, 2000);
        assertNotNull(rangeSeekBar.getLeftSeekBar());
        assertEquals(81, (int) rangeSeekBar.getLeftSeekBar().getProgress(), 3000);
    }

    @Test
    public void testVerticalSeekBar() {
        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tabList);
        iAbilityDelegator.runOnUIThreadSync(() -> tabList.selectTabAt(3));
        Component component = ability.fractionContainer.getComponentAt(0);
        RangeSeekBar rangeSeekBar = (RangeSeekBar) component.findComponentById(ResourceTable.Id_sb_vertical_1);
        AtomicInteger width = new AtomicInteger();
        iAbilityDelegator.runOnUIThreadSync(() ->
                width.set(rangeSeekBar.getHeight())
        );
        EventHelper.inputSwipe(ability, rangeSeekBar, 60, (int) (width.get() * 0.1f), 60, (int) (width.get() * 0.9f), 2000);
        assertNotNull(rangeSeekBar.getLeftSeekBar());
        assertEquals(6, (int) rangeSeekBar.getLeftSeekBar().getProgress(), 3000);
    }
}